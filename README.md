## Arduino Water Heater IoT App

This repo corresponds to the JS code meant to run in the Lunix side of the Arduino Yun.
Currently implemented using Cylon.js.
Tested with node 0.10.33.

Notes:

* When using AWS IoT 1.0.10 the return statement from line 268 of thing/index.js needs
to be commented out in order to receive messages with external origin.
* To reduce the footprint of cylon-firmata, cylon-i2c can be manually removed from the
dependencies array found in cylon-firmata/index.js.