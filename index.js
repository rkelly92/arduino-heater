var Cylon = require('cylon'),
  Promise = require('bluebird'),
  _ = require('lodash'),
  awsIot = require('aws-iot-device-sdk');

var SUPPLY_VOLTAGE = 120;

Cylon.robot({
  connections: {
    arduino: { adaptor: 'firmata', port: '/dev/ttyATH0' }
  },

  devices: {
    waterTemperatureSensor: { driver: 'ds18b20', pin: 2, module: 'cylon-one-wire' },
    currentSensor: { driver: 'analog-sensor', pin: 0 },
    relay: { driver: 'relay', pin: 3 }
  },

  getEffectivePower: function () {
    var numberOfMeasurements = 25,
      delay = 20,
      measurementPromises = [];

    var getCurrent = function (count) {
      return (Math.abs(count - 512) / 1024) * (5 / 0.066);
    };

    for (var i = 1; i <= numberOfMeasurements; i++) {
      measurementPromises.push(Promise.delay(delay * i)
        .then(function () {
          return this.currentSensor.analogRead();
        }.bind(this)));
    }

    return Promise.all(measurementPromises)
      .then(function (measurements) {
        console.log(measurements);

        // Get peaks of analog wave.
        var max = getCurrent(_.max(measurements));
        var min = getCurrent(_.min(measurements));

        // Use the mean of the peak values to try to eliminate any noise.
        var effectiveCurrentRms = _.mean([max, min]) / Math.sqrt(2);

        return effectiveCurrentRms * SUPPLY_VOLTAGE;
      });
  },

  work: function (my) {
    // For some reason, if the connection is done outside this scope it doesn't work.
    var thingShadows = awsIot.thingShadow({
      keyPath: '../certificates/b954e6c1e1-private.pem',
      certPath: '../certificates/b954e6c1e1-certificate.pem',
      caPath: '../certificates/rootCA.pem',
      clientId: 'arduino-yun-b954e6c1e1',
      region: 'us-east-1'
    });

    var clientTokenInitialGet;
    var clientTokenUpdate;
    var WATER_HEATER_THING_NAME = 'water-heater';

    thingShadows.on('connect', function () {
      console.log('MQTT connection established!');

      thingShadows.register(WATER_HEATER_THING_NAME);

      setTimeout(function () {
        clientTokenInitialGet = thingShadows.get(WATER_HEATER_THING_NAME);

        my.waterTemperatureSensor.readTemperature(function (error, temperature) {
          if (error) {
            console.log(error);
          }

          clientTokenUpdate = thingShadows.update(WATER_HEATER_THING_NAME, {
            state: {
              reported: {
                waterTemperature: temperature
              }
            }
          });
        });

        if (clientTokenInitialGet === null) {
          console.log('Initial get call failed, operation still in progress.');
        }

        if (clientTokenUpdate === null) {
          console.log('Initial update call failed, operation still in progress.')
        }
      }, 5000);
    });

    thingShadows.on('status', function (thingName, statusType, clientToken, stateObject) {
      if (statusType === 'rejected') {
        console.log('Rejected operation.');
        console.log(stateObject);

        if (stateObject.code === '404') {
          console.log('Creating device shadow.');
          my.waterTemperatureSensor.readTemperature(function (error, temperature) {
            if (error) {
              console.log(error);
            }

            clientTokenUpdate = thingShadows.update(WATER_HEATER_THING_NAME, {
              state: {
                reported: {
                  waterTemperature: temperature,
                  enabled: my.relay.isOn
                }
              }
            });
          });
        }
      }

      if (statusType === 'accepted') {
        if (thingName === WATER_HEATER_THING_NAME) {
          if (clientToken === clientTokenInitialGet) {

            // This is the response for an initial get.
            // The relay is closed (not on) by default.
            if (_.get(stateObject.state, 'desired.enabled') === true) {
              my.relay.turnOn();
            }

            every((19500).milliseconds(), function () {
              if (my.relay.isOn) {
                my.getEffectivePower()
                  .then(function (effectivePower) {
                    var message = {
                      power: effectivePower,
                      timestamp: Date.now(),
                      deviceId: 'Yun-ACS712'
                    };

                    thingShadows.publish('my/topic', JSON.stringify(message));
                  })
                  .catch(function (error) {
                    console.log('Error getting effective power.');
                    console.log(error);
                  });
              }
            });

            every((20).seconds(), function () {
              my.waterTemperatureSensor.readTemperature(function (error, temperature) {
                if (error) {
                  console.log(error);
                }

                clientTokenUpdate = thingShadows.update(WATER_HEATER_THING_NAME, {
                  state: {
                    reported: {
                      waterTemperature: temperature
                    }
                  }
                });
              });
            });
          }

          // External update.
          if (clientToken !== clientTokenUpdate) {
            if (_.get(stateObject.state, 'desired.enabled') === false && my.relay.isOn) {
              my.relay.turnOff();
              console.log('Relay is on: ', my.relay.isOn);
            }
          }
        }
      }
    });

    thingShadows.on('delta', function (thingName, stateObject) {
      if (thingName === WATER_HEATER_THING_NAME) {
        // Deltas should only happen for true.
        // The implementation forces reported to always be false.
        if (stateObject.state.enabled === true && !my.relay.isOn) {
          my.relay.turnOn();
          console.log('Relay is on: ', my.relay.isOn);
        }
      }
    });

    thingShadows.on('delta', function (thingName, stateObject) {
      if (thingName === WATER_HEATER_THING_NAME) {
        console.log(JSON.stringify(stateObject, null, 4));
        my.relay.toggle();
      }
    });
  }
}).start();
