var awsIot = require('aws-iot-device-sdk');

var device = awsIot.device({
  keyPath: '../certificates/b954e6c1e1-private.pem',
  certPath: '../certificates/b954e6c1e1-certificate.pem',
  caPath: '../certificates/rootCA.pem',
  clientId: 'arduino-yun-b954e6c1e1',
  region: 'us-east-1'
});

device.on('connect', function() {
  console.log('connect');
  device.subscribe('topic_1');
  device.publish('topic_2', JSON.stringify({ test_data: 1 }));
});

device.on('message', function(topic, payload) {
  console.log('message ', topic);
  console.log(typeof payload.toString());
  console.log(typeof payload);
});
